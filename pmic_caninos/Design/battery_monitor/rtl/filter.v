/*
@file:filter.v
@Author: Igor Ruschi Andrade E Lima
@contat: igor.lima@lsitec.org

@Description: Filter block to remove noise from ADC
@clk input of 32khz
@rst_n assyncronous reset
@en enable/disable filter
@out input filtered
@in input from adc
@ready indicate that the output is avaliable
*/

`timescale 100 ns/100 ns



module filter
	#(parameter ADC_SIZE = 16,
		    COUNTER_SIZE = 2,
		    FILTER_SIZE = 3)
(	
input 		clk, 
input 		rst_n, 
input 		en, 
input		[ADC_SIZE-1:0]in,
output	reg	[ADC_SIZE-1:0]out,
output reg	ready
);

reg [ADC_SIZE+3:0] filt_data, next_filt_data;
reg [COUNTER_SIZE-1:0] count, next_count;

always@(posedge clk, negedge rst_n)
begin
	if(!rst_n)
	begin
		count <= 0;
		filt_data <= in;
		ready <= 0;
	end
	else begin

		if(en)begin
			
			if(count == FILTER_SIZE)
			begin
				filt_data <= filt_data;
				ready <= 1;
				count <= count;
			end
			else
			begin
				count <= next_count;
				filt_data <= next_filt_data;
				ready <= 0;
			end

		end
	
	end
end

always@*
begin
	next_filt_data = (filt_data + in) / 2;
	next_count = count + 1;
	if(ready)
		out = filt_data[ADC_SIZE-1:0];
	else
		out = 0;
end






endmodule
