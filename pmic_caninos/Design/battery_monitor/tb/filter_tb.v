/*
@file: filter_tb.v
@Author: Igor Ruschi Andrade E Lima
@contat: igor.lima@lsitec.org

@Descriptio

*/


`timescale 100 ns/100 ns  // time-unit = 1 ns, precision = 10 ps
//`include "Ruschigo/PMIC_DIGITAL/Design/Timer/timer_full.v"

module filter_tb();

localparam period = 15625;
localparam clock = 2*period;  

reg 		clk;
reg 		rst_n; 
reg 		en;
reg		[15:0]in;
wire 		[15:0]out;
wire		ready;

initial begin        
	$display ("time\t | clk | rst_n|  en|  out");	
	$monitor ("%g\t   | %b  |   %b |  %b|  %b|", $time, clk, rst_n, en, out);
	$display ("{1'b1, {8-1{1'b1}}} = 0b%b", {1'b1, {8-1{1'b1}}});	
	clk = 1;       // initial value of clock
	rst_n = 1;       // initial value of reset
	en = 0;      // initial value of enable
	rst_n = 0;    // Assert the reset
	in = 500;
	#clock;
	rst_n = 1;   // De-assert the reset
	#clock;
	en = 1;  // Assert enable
	#clock;
	in = 600;
	#clock;
	in = 700;
	#clock;
	in = 800;
	#clock;
	wait(ready);
	#clock;
	en = 0; // De-assert enable
	rst_n = 0;
	#clock;
	rst_n = 1;
	#clock;
	repeat (10)
		#clock;
	$finish;      // Terminate simulation
end

always //clock gen
begin
	clk = ~clk;
	#period;
end

//Design under Test
filter DUT
(
clk,
rst_n,
en,
in,
out,
ready
);

endmodule
