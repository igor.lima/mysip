/*
@file: reg_bank_tb.v
@Author: Igor Ruschi Andrade E Lima
@contat: igor.lima@lsitec.org

@Description: Testbench for Register Bank
*/


`timescale 1 ns/ 1 ns  // time-unit = 1 ns, precision = 1 ns

module regBank_tb();

localparam	period = 15625;
localparam 	REG_SIZE = 32;
localparam	ADDR_SIZE = 32;
localparam	BANK_SIZE = 100;  

reg 			clk;
reg 			rst_n;
reg 			write_en;
reg	[ADDR_SIZE-1:0]	rdAddr;
reg	[ADDR_SIZE-1:0]	wrAddr;
reg	[REG_SIZE-1:0]	wrData;
wire	[REG_SIZE-1:0]	rdData;

reg	[REG_SIZE-1:0]	counter;
initial begin        
	$display ("time\t | clk | rst_n|  wr_en|  rdData");	
	$monitor ("%g\t   | %b  |   %b |    %b |   %d", $time, clk, rst_n, write_en, rdData);
	//$display ("{1'b1, {8-1{1'b1}}} = 0b%b", {1'b1, {8-1{1'b1}}});	
	clk = 1;       // initial value of clock
	rst_n = 1;       // initial value of reset
	write_en = 0;      // initial value of enable
	rst_n = 0;    // Assert the reset
	#(period*2);
	//#period
	rst_n = 1;   // De-assert the reset
	#(period * 2);
	
	counter = 0;
	repeat (100)		
	begin
		write_en = 0;
		wrAddr = counter;
		wrData = counter;
		#(period * 2);
		write_en = 1;  // Assert wr enable		
		#(period * 2);
		counter = counter + 1;
	end
	write_en = 0;
	counter = 0;
	repeat (100)		
	begin
		rdAddr = counter;
		#(period * 2);
		counter = counter + 1;
	end

	rst_n = 0;
	#period;
	#period;
	rst_n = 1;
	#period;
	repeat (100)
		#period;
	$finish;      // Terminate simulation
end

always //clock gen
begin
	clk = ~clk;
	#period;
end

//Design under Test
regBank #(32, 32, 100) DUT
(
clk,
rst_n,
write_en,
rdAddr,
wrAddr,
wrData,
rdData,
);

endmodule
