/*
@file:reg_bank.v
@Author: Igor Ruschi Andrade E Lima
@contat: igor.lima@lsitec.org

@Description: Register File for PMIC DIGITAL stuffs, contain all registers configurable by user's interface or other general registers used by multiple blocks
@clk input of 32khz
@rst_n assyncronous reset
@write_en
@rdAddr
@rdData
@wrAddr
@wrData
*/

`timescale 1 ns/ 1ns

module regBank
	#(parameter 	REG_SIZE = 32,
		    	ADDR_SIZE = 32,
			BANK_SIZE = 100)
(	
input 			clk, 
input 			rst_n,
input			write_en, 
input	[ADDR_SIZE-1:0]	rdAddr,
input	[ADDR_SIZE-1:0]	wrAddr,
input	[REG_SIZE-1:0]	wrData,
output	[REG_SIZE-1:0]	rdData
);

//create a register matrix of registers
reg 	[REG_SIZE-1:0]	reg_bank	[0:BANK_SIZE-1];

integer 		i;

always @(posedge clk, negedge rst_n)
begin
	if(!rst_n)
	begin	
		for (i = 0; i < BANK_SIZE; i = i + 1) begin
	    		reg_bank[i] <= 0;
	 	end
		/*if is necessary to initiate especific registers do it just here*/
	end
	else
	begin
		if(write_en) reg_bank[wrAddr] <= wrData;
	end

end

assign rdData = reg_bank[rdAddr]; 


endmodule
